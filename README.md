# Przeczytaj mnie
Wtyczka ta została zaprojektowa w taki sposób aby była kompletnie niezależna od motywu oraz innych wtyczke. Należy jednak zaznaczyć że nazwy klas oraz identyfikatory mogą się powtórzyć, wtedy możliwy jest konflikt. Jest to jednak mało prawodpodobne ponieważ zastosowano namespace'y. 
## Wymagania
  - Wordpress > 3.3

## Instalacja
Wtyczkę można zainstalować na dwa sposoby: 
> Poprzez wgranie folderu do ``$root/wp-content/plugins/`` i aktywowanie w ``Kokpit/Wtyczki/Helikon Form/Aktywacja``

>Lub poprzez ``Kokpit/Wtyczki/Dodaj nową/`` wybieramy archiwum .zip i wgrywamy.
>Następnie aktywujemy wtyczkę.

## Przed korzystaniem !ważne!
Przed korzystanie z wtyczki należy przejść do zakładki ``Kokpit/Helikon/Opcje`` formularza i uzupełnić wszystkie pola w odpowiedni sposób (we wszystkich zakładkach, ** przed zmianą zakładki proszę zapisać zmiany ** ) a następnie zapisać. 
** Gotowe, wtyczke jest gotowa do użycia ** 

## Korzystanie
Aby wyświetlić formularz należy w oknie edycji wpisu lub strony dodać shortcode ``[formularz]``. Aby dodać przycisk do przejechania do formularza wpisujemy ``[zobacz]``

#### * Automatyczna aktualizacja
Wtyczka ma możliwość automatycznej aktualizacji to znaczy, jeżeli wtyczka zostanie zaktualizowana w repozytorium to w każdym z zainstalowych instancji wordpressa pojawi się informacja o możliwości aktualizacji poprzez standardowy system aktyalizacji. 

** Automatyczna aktualizacja wymaga wtyczki GitHub Updater **
* [Pobierz] - pobierz GitHub Updater
* [Oficjalne repozytorium] - zobacz repozytorium

[Pobierz]:https://github.com/afragen/github-updater/archive/develop.zip
[Oficjalne repozytorium]:https://github.com/afragen/github-updater