<?php
if (!class_exists('AdminPageFramework')) {
    include (dirname(__FILE__) . '/apf/library/admin-page-framework.min.php');
}
class MS_Options extends AdminPageFramework {

    public function setUp() {
        $this->setRootMenuPage( 'Helikon' );
        $this->addSubMenuItem(
            array(
                'title'     => 'Opcje formularza',
                'page_slug' => 'helikon_form'
            )
        );
    }
    public function load_helikon_form( $oAdminPage ) {
      $this->addInPageTabs(
          'helikon_form',
          array(
              'tab_slug'    =>    'ms_text',
              'title'        =>    'Treść',
          ),
          array(
              'tab_slug'    =>    'ms_style',
              'title'        =>    'Wygląd',
          ),
          array(
              'tab_slug'    =>    'ms_devs',
              'title'        =>    'Dev',
          )
      );
      $this->addSettingSections(
           array(
               'section_id'    => 'ms_content',
               'page_slug'     => 'helikon_form',
               'tab_slug'      => 'ms_text',
               'title'         => 'Treść',
               'description'   => 'Proszę wypełnić odpowiednie pola tekstami.',
               'order'         => 10,
           ),
           array(
               'section_id'    => 'ms_design',
               'page_slug'     => 'helikon_form',
               'tab_slug'      => 'ms_style',
               'title'         => 'Wygląd',
               'description'   => 'Zmiany wyglądu formularza.',
           ),
           array(
               'section_id'    => 'ms_dev',
               'page_slug'     => 'helikon_form',
               'tab_slug'      => 'ms_devs',
               'title'         => 'Developer',
               'description'   => 'Zmiany zaawansowane.',
           )
      );
      $this->addSettingFields(
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_oczekiwania',
              'type'          => 'text',
              'title'         => 'Oczekiwania',
              'description'   => 'Oczekiwania',
              'default' => 'Oczekiwana kwota kredytu (wpisz w zł)'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_rodzaj_kredytu',
              'type'          => 'text',
              'title'         => 'Rodzaj kredytu',
              'description'   => 'Wybierz rodzaj kredytu',
              'default' => 'Rodzaj kredytu:'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id' => 'ms_rodzaje_kredytu',
              'type' => 'text',
              'title' => 'Rodzaje kredytu',
              'repeatable' => true,
              'sortable' => true,
              'default' => array('Chwilówka', 'Kredyt/pożyczka gotówkowa', 'Kredyt konsolidacyjny', 'Kredyt dla firm')
          ),
          array(
                'section_id' => 'ms_content',
              'field_id' => 'ms_miesieczny',
              'type' => 'text',
              'title' => 'Miesięczny dochód',
              'description'   => 'Wpisz miesięczny dochód',
              'default' => 'Miesięczny dochód NETTO (wpisz w zł)'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id' => 'ms_miejscowosc',
              'type' => 'text',
              'title' => 'Miejscowość',
              'description'   => 'Wpisz miejscowosc',
              'default' => 'Miejscowość'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id' => 'ms_kod_pocztowy',
              'type' => 'text',
              'title' => 'Kod pocztowy',
              'description'   => 'Wpisz kod pocztowy',
              'default' => 'Kod pocztowy'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id' => 'ms_telefon',
              'type' => 'text',
              'title' => 'Telefon',
              'description'   => 'Wpisz telefon',
              'default' => 'Telefon'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_preferowane_godziny_kontaktu',
              'type'          => 'text',
              'title'         => 'Preferowane godziny kontaktu',
              'default' => 'Preferowane godziny kontaktu'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id' => 'ms_godziny',
              'type' => 'text',
              'title' => 'Preferowane godziny kontaktu',
              'repeatable' => true,
              'sortable' => true,
          ),
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_uwagi',
              'type'          => 'text',
              'title'         => 'Uwagi',
              'default' => 'Uwagi'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_obowiazkowe',
              'type'          => 'textarea',
              'title'         => 'Obowiazkowe',
              'default' => 'Pole obowiązkowe'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_polityka',
              'type'          => 'textarea',
              'title'         => 'Polityka prywatności',
              'default' => 'Przyjmuję do wiadomości, że podane przeze mnie informacje mają wyłącznie charakter kontaktowy i nie stanowią danych osobowych w rozumieniu Ustawy z dnia 29 sierpnia 2002 r. o ochronie danych osobowych.<br> W celu przygotowania oferty kredytowej skontaktujemy się z Państwem telefonicznie.'
          ),
          array(
              'section_id' => 'ms_content',
              'field_id'      => 'ms_submit',
              'type'          => 'text',
              'title'         => 'Tekst przycisku wyślij',
              'default' => 'Złóż wniosek'
          ),
          array(
              'section_id' => 'ms_content',
              'field_id'      => 'ms_wniosek',
              'type'          => 'text',
              'title'         => 'Tekst przycisku Złóż wniosek',
              'default' => 'Złóż wniosek'
          ),
          array(
                'section_id' => 'ms_content',
              'field_id'      => 'ms_submit_button',
              'type'          => 'submit',
              'value'=> "Zapisz"
          )
      );
      $this->addSettingFields(
        array(
            'section_id'    => 'ms_design',
            'field_id'      => 'ms_submitBg',
            'type'          => 'color',
            'title'         => 'Tło przycisku'
        ),
        array(
            'section_id'    => 'ms_design',
            'field_id'      => 'ms_borderColor',
            'type'          => 'color',
            'title'         => 'Kolor obramowania'
        ),
        array(
            'section_id'    => 'ms_design',
            'field_id'      => 'ms_submitColor',
            'type'          => 'color',
            'title'         => 'Kolor tekstu przycisku',
        ),
        array(
            'section_id'    => 'ms_design',
            'field_id'      => 'ms_requiredColor',
            'type'          => 'color',
            'title'         => 'Wymagane (kolor gwiazdki)',
        ),
        array(
            'section_id'    => 'ms_design',
            'field_id'      => 'ms_submit_button',
            'type'          => 'submit',
            'label'=> "Zapisz"
        )
      );
      $this->addSettingFields(
        array(
            'section_id'    => 'ms_dev',
            'field_id'      => 'ms_submit_link',
            'type'          => 'text',
            'title'         => 'Adres gdzie ma być przekazany formularz po submicie'
        ),
        array(
            'section_id'    => 'ms_dev',
            'field_id'      => 'ms_submit_method',
            'type'          => 'select',
            'label'     =>  array(
                'post' =>  'POST',
                'get' =>  'GET',
              ),
            'title'         => 'Metoda przesyłania danych z formularza'
        ),
        array(
            'section_id'    => 'ms_dev',
            'field_id'      => 'ms_submit_button',
            'type'          => 'submit',
            'label'=> "Zapisz"
        )
      );
    }
}
new MS_Options;

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://psmarcin.github.io
 * @since      1.0.0
 *
 * @package    Helikon_Form
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Helikon_Form
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class Plugin_Name
{
    
    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Plugin_Name_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;
    
    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;
    
    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;
    
    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the Dashboard and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {
        
        $this->plugin_name = 'helikon-form';
        $this->version = '1.0.0';
        
        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        $this->load_options();
        $this->form_shortcode();
        $this->scroll_shortcode();
    }
    
    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Plugin_Name_Loader. Orchestrates the hooks of the plugin.
     * - Plugin_Name_i18n. Defines internationalization functionality.
     * - Plugin_Name_Admin. Defines all hooks for the dashboard.
     * - Plugin_Name_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {
        
        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-helikon-form-loader.php';
        
        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-helikon-form-i18n.php';
        
        /**
         * The class responsible for defining all actions that occur in the Dashboard.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-helikon-form-admin.php';
        
        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-helikon-form-public.php';
        
        $this->loader = new Plugin_Name_Loader();
    }
    
    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Plugin_Name_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {
        
        $plugin_i18n = new Plugin_Name_i18n();
        $plugin_i18n->set_domain($this->get_plugin_name());
        
        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }
    
    /**
     * Register all of the hooks related to the dashboard functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {
        
        $plugin_admin = new Plugin_Name_Admin($this->get_plugin_name(), $this->get_version());
        
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
    }
    
    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {
        
        $plugin_public = new Plugin_Name_Public($this->get_plugin_name(), $this->get_version());
        
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
    }
    
    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }
    
    public function load_options() {
      add_action( 'wp_head', 'form_style');
    }
    
    /**
     * Add shortcode support
     * @return boolean If success
     */
    public function form_shortcode() {
        add_shortcode('formularz', 'form_core');
    }
    
    /**
     * Add shortcode support
     * @return boolean If success
     */
    public function scroll_shortcode() {
        add_shortcode('zobacz', 'zobaczFormularz');
    }
    
    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }
    
    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Plugin_Name_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }
    
    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }
}

function form_style(){
  $dataStyle = get_option( 'MS_Options', array() )['ms_design']; 
  ?>
  <style>
    .ms_form#ms_form label span{
      color: <?php echo $dataStyle['ms_requiredColor'] ?>;
    }
  </style>
  <?php
}

function form_core($atts) {
    $data = get_option( 'MS_Options', array() );
    $dataContent = $data['ms_content'];
    $dataDev = $data['ms_dev'];
    $dataStyle = $data['ms_design'];
    $rodzajeString = '<option value="">Wybierz</option>';
    foreach ($dataContent['ms_rodzaje_kredytu'] as $rodzaj) {
      $rodzajeString.= "<option value=".$rodzaj.">".$rodzaj."</option>";
    }
    $godzinyString = '<option value="">Wybierz</option>';
    foreach ($dataContent['ms_godziny'] as $godziny) {
      $godzinyString.= "<option value=".$godziny.">".$godziny."</option>";
    }
    return '
        <div class="ms_form row" id="ms_form">
          <div class="ms-small-12 columns">
            <form action="'.$dataDev['ms_submit_link'].'" method="'.$dataDev['ms_submit_method'].'" data-abide novalidate="novalidate">
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="kwota"><span>*</span>' . $dataContent['ms_oczekiwania'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <input class="ms-input" type="number" id="kwota" data-validation="required" data-validation="number" data-validation-allowing="int" data-validation-allowing="range[100;10000] "name="kwota">
                </div>
              </div>
              <div class="ms-row">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="rodzaj"><span>*</span>' . $dataContent['ms_rodzaj_kredytu'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <select class="ms-select" id="rodzaj" data-validation="required" name="rodzaj">
                    ' . $rodzajeString . '
                  </select>
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="netto"><span>*</span>' . $dataContent['ms_miesieczny'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <input class="ms-input" type="number" data-validation="required" id="netto" data-validation="number" data-validation-allowing="int" name="netto">
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="miejscowosc"><span>*</span>' . $dataContent['ms_miejscowosc'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <input class="ms-input" type="text" data-validation="required" id="miejscowosc" name="miejscowosc">
                </div>
              </div>
              <div class="ms-row ms-callapsed kodPocztowy">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="kod-pocztowy"><span>*</span>' . $dataContent['ms_kod_pocztowy'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <input class="ms-input" type="text" data-validation="length" data-validation-length="2-2" id="kod-pocztowy" name="kodPoczatek"> - <input class="ms-input" type="text" name="kodKoniec" data-validation="length" data-validation-length="3-3" >
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="telefon"><span>*</span>' . $dataContent['ms_telefon'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <input class="ms-input" type="tel" id="telefon" data-validation="custom" data-validation-regexp="^[0-9]{9}" name="telefon">
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="godzina"><span>*</span>' . $dataContent['ms_preferowane_godziny_kontaktu'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <select class="ms-select" id="godzina" data-validation="required" name="godzina">
                    ' . $godzinyString . '
                  </select>
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">
                  <label class="ms-label" for="uwagi">' . $dataContent['ms_uwagi'] . ':</label>
                </div>
                <div class="ms-small-6 ms-columns">
                  <textarea class="ms-textarea" name="uwagi" id="uwagi"></textarea>
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-6 ms-columns">

                </div>
                <div class="ms-small-6 ms-columns obowiazkowe">
                  <label class="ms-label"><span>*</span> <i>' . $dataContent['ms_obowiazkowe'] . '</i></label>
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                <div class="ms-small-1 ms-columns">
                  <input class="ms-input" type="checkbox" data-validation="required" name="polityka" id="polityka">
                </div>
                <div class="ms-small-11 ms-columns obowiazkowe">
                  <label class="ms-label" for="polityka" class="polityka"><i>' . $dataContent['ms_polityka'] . '</i></label>
                </div>
              </div>
              <div class="ms-row ms-callapsed">
                  <input class="ms-input" type="submit" onsubmit="return validateForm();" class="ms-button" style="background-color: '.$dataStyle['ms_submitBg'].'; color: '.$dataStyle['ms_submitColor'].'; border-color: '.$dataStyle['ms_borderColor'].'" value="'.$dataContent['ms_submit'].'">
              </div>
            </form>
          </div>
        </div>
    ';
}
function zobaczFormularzScript(){
  ?>
    <script>
      jQuery(document).ready(function($) {
        if ($(".ms_scrollToForm").length) {
          $(".ms_scrollToForm").on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#ms_form").offset().top - 50
            }, 1000);
          });
        };
        var polish = {
              errorTitle : 'Błąd podczas walidacji formularza',
              requiredFields : 'Nie wpisałeś wszystkich wymaganych pól',
              badTime : 'Wprowadziłeś błędny czas',
              badEmail : 'Wprowadziłeś błędny adres email',
              badTelephone : 'Wprowadziłeś błędny numer telefonu',
              badSecurityAnswer : 'Wprowadziłeś błędny kod weryfikacyjny',
              badDate : 'Wprowadziłeś błędną datę',
              lengthBadStart : 'Musisz wprowadzić wartość pomiędzy ',
              lengthBadEnd : ' znaków',
              lengthTooLongStart : 'Wprowadziłeś wartość dłuższą od ',
              lengthTooShortStart : 'YWprowadziłeś wartość krótszą od ',
              notConfirmed : 'Wartości nie mogą być sprawdzone',
              badDomain : 'Błędna domena',
              badUrl : 'Wartość którą wprowadziłeś nie jest poprawną formą adresu URL',
              badCustomVal : 'Błędny format ',
              badInt : 'Wartość którą wprowadziłeś nie jest poprawną liczbą całkowitą',
              badSecurityNumber : 'Błędny kod',
              badUKVatAnswer : 'Niepoprawny UK VAT',
              badStrength : 'Hasło nie jest wystarczająco mocne ',
              badNumberOfSelectedOptionsStart : 'Musisz wybrać co najmniej ',
              badNumberOfSelectedOptionsEnd : ' odpowiedzi',
              badAlphaNumeric : 'W odpowiedzi mogą znaleźć się tylko znaki alfanumeryczne ',
              badAlphaNumericExtra: ' i ',
              wrongFileSize : 'The file you are trying to upload is too large',
              wrongFileType : 'The file you are trying to upload is of wrong type',
              groupCheckedRangeStart : 'Wybierz pomiedzy ',
              groupCheckedTooFewStart : 'Wybierz conajmniej ',
              groupCheckedTooManyStart : 'Wybierz maksymalnie ',
              groupCheckedEnd : ''
            };
        $.validate({
          language: polish
        });
      });
    </script>
  <?php
}
function zobaczFormularz( $atts ){
    add_action( 'wp_footer', 'zobaczFormularzScript' );
    $dataStyle = get_option( 'MS_Options', array() )['ms_design']; 
    $dataContent = get_option( 'MS_Options', array() )['ms_content']; 
    return "<a href=\"#ms_form\" class=\"ms-button aligncenter signUp ms_scrollToForm\" style='background-color: ".$dataStyle["ms_submitBg"]."; color: ".$dataStyle["ms_submitColor"]."; border-color: ".$dataStyle["ms_borderColor"]."'>".$dataContent['ms_wniosek']."</a>";
}
